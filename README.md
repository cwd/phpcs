The cwd.at Coding Standard
==========================

This is the coding standard of projects developed by the cwd.at GmbH. You can
automatically enforce the coding standard by running [PHP-CS-Fixer].

Installation
------------

Install the coding standard with Composer:

~~~
$ composer require --dev cwd/phpcs:dev-master
~~~

Usage
-----

Place the following `.php_cs` file in the root directory of your project:

~~~
<?php

$finder = Symfony\CS\Finder::create()
    ->in([__DIR__.'/src', __DIR__.'/tests'])
;

Symfony\CS\Fixer\Contrib\HeaderCommentFixer::setHeader(<<<EOF
This file is part of the <package name>.

(c) cwd.at GmbH <office@cwd.at>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
EOF
);

return Symfony\CS\Config::create()
    ->setUsingCache(true)
    ->addCustomFixers(Cwd\PhpCs\CodingStandard::getCustomFixers())
    ->fixers(Cwd\PhpCs\CodingStandard::PHP5_FIXERS)
    ->finder($finder)
;
~~~

Two different sets of fixers are available:

* `Cwd\PhpCs\CodingStandard::PHP5_FIXERS` (for PHP 5.4+)
* `Cwd\PhpCs\CodingStandard::PHP7_FIXERS` (for PHP 7.0+)

Enforce the coding style by running PHP-CS-Fixer:

~~~
$ vendor/bin/php-cs-fixer fix
~~~

Issues
------

Report issues on the [issue tracker].


[PHP-CS-Fixer]: https://github.com/FriendsOfPHP/PHP-CS-Fixer
[issue tracker]: https://gitlab.cwd.at/cwd/phpcs/issues
