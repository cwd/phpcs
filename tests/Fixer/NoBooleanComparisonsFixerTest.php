<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Tests\Fixer;

class NoBooleanComparisonsFixerTest extends AbstractFixerTest
{
    /**
     * @dataProvider provideCases
     */
    public function testFix($expected, $input = null)
    {
        $this->makeTest($expected, $input);
    }

    public function provideCases()
    {
        return [
            [
                    <<<'EOF'
<?php

if ($condition) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if (true === $condition) {
    echo "Do something";
}
EOF
            ],
            [
                    <<<'EOF'
<?php

if (!$condition) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if (true !== $condition) {
    echo "Do something";
}
EOF
            ],
            [
                    <<<'EOF'
<?php

if ($condition) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if (true == $condition) {
    echo "Do something";
}
EOF
            ],
            [
                    <<<'EOF'
<?php

if (!$condition) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if (true != $condition) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if ($condition) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if ($condition === true) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if (!$condition) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if ($condition !== true) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if ($foo && $bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if ($foo && $bar === true) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if (!$bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if (false === $bar) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if ($bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if (false !== $bar) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if (!$bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if (false == $bar) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if ($bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if (false != $bar) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if (!$bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if ($bar === false) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if ($bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if ($bar !== false) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if (!$bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if ($bar == false) {
    echo "Do something";
}
EOF
            ],
            [
                <<<'EOF'
<?php

if ($bar) {
    echo "Do something";
}
EOF
                    ,
                    <<<'EOF'
<?php

if ($bar != false) {
    echo "Do something";
}
EOF
            ],
        ];
    }
}
