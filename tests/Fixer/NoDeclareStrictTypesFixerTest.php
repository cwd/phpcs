<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Tests\Fixer;

class NoDeclareStrictTypesFixerTest extends AbstractFixerTest
{
    /**
     * @dataProvider provideCases
     */
    public function testFix($expected, $input = null)
    {
        if (PHP_VERSION_ID < 70000) {
            $this->markTestSkipped('Only supported on PHP 7');
        }

        $this->makeTest($expected, $input);
    }

    public function provideCases()
    {
        return [
            [
                <<<'EOF'
<?php

class Foo extends Bar
{
}
EOF
                    ,
                    <<<'EOF'
<?php

declare(strict_types=1);

class Foo extends Bar
{
}
EOF
            ],
            [
                <<<'EOF'
<?php

/**
 * Class comment
 */
class Foo extends Bar
{
}
EOF
                    ,
                    <<<'EOF'
<?php

declare(strict_types=1);

/**
 * Class comment
 */
class Foo extends Bar
{
}
EOF
            ],
            [
                <<<'EOF'
<?php

/*
 * File comment
 */

class Foo extends Bar
{
}
EOF
                    ,
                    <<<'EOF'
<?php

/*
 * File comment
 */

declare(strict_types=1);

class Foo extends Bar
{
}
EOF
            ],
        ];
    }
}
