<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Tests\Fixer;

class ImportGlobalClassesFixerTest extends AbstractFixerTest
{
    /**
     * @dataProvider provideCases
     */
    public function testFix($expected, $input = null)
    {
        $this->makeTest($expected, $input);
    }

    public function provideCases()
    {
        return [
            [
                <<<'EOF'
<?php

use Bar;
use Some\Clazz;

class Foo extends Bar
{
}
EOF
                    ,
                    <<<'EOF'
<?php

use Some\Clazz;

class Foo extends \Bar
{
}
EOF
            ],
            [
                <<<'EOF'
<?php

use Bar;

class Foo extends Bar
{
}
EOF
                    ,
                    <<<'EOF'
<?php

class Foo extends \Bar
{
}
EOF
            ],
            [
                <<<'EOF'
<?php

use Bar;

/**
 * Class description
 */
class Foo extends Bar
{
}
EOF
                    ,
                    <<<'EOF'
<?php

/**
 * Class description
 */
class Foo extends \Bar
{
}
EOF
            ],
            [
                    <<<'EOF'
<?php

use Foo;

$x = Foo::BAR;
EOF
                    ,
                    <<<'EOF'
<?php

$x = \Foo::BAR;
EOF
            ],
            [
                    <<<'EOF'
<?php

/**
 * File header
 */

use Foo;

$x = Foo::BAR;
EOF
                    ,
                    <<<'EOF'
<?php

/**
 * File header
 */

$x = \Foo::BAR;
EOF
            ],
            [
                    <<<'EOF'
<?php

use Foo;

function f($foo = Foo::BAR) {
}
EOF
                    ,
                    <<<'EOF'
<?php

function f($foo = \Foo::BAR) {
}
EOF
            ],
            [
                    <<<'EOF'
<?php

use Foo;

function f(Foo $foo) {
}
EOF
                    ,
                    <<<'EOF'
<?php

function f(\Foo $foo) {
}
EOF
            ],
            [
                    <<<'EOF'
<?php

use Foo;

$foo = new Foo();
EOF
                    ,
                    <<<'EOF'
<?php

$foo = new \Foo();
EOF
            ],
            [
                    <<<'EOF'
<?php

use Foo;

/**
 * @param Foo $foo Description
 */
function f(Foo $foo) {
}
EOF
                    ,
                    <<<'EOF'
<?php

use Foo;

/**
 * @param \Foo $foo Description
 */
function f(Foo $foo) {
}
EOF
            ],
            [
                    <<<'EOF'
<?php

use Foo;

/**
 * @return Foo Description
 */
function f() {
}
EOF
                    ,
                    <<<'EOF'
<?php

/**
 * @return \Foo Description
 */
function f() {
}
EOF
            ],
        ];
    }
}
