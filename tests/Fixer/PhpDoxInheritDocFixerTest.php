<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Tests\Fixer;

class PhpDoxInheritDocFixerTest extends AbstractFixerTest
{
    /**
     * @dataProvider provideCases
     */
    public function testFix($expected, $input = null)
    {
        $this->makeTest($expected, $input);
    }

    public function provideCases()
    {
        return [
            [
                <<<'EOF'
<?php

/**
 * @inheritdoc
 */
class A
{
}
EOF
                    ,
                    <<<'EOF'
<?php

/**
 * {@inheritdoc}
 */
class A
{
}
EOF
            ],
        ];
    }
}
