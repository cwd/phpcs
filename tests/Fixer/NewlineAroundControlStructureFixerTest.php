<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Tests\Fixer;

class NewlineAroundControlStructureFixerTest extends AbstractFixerTest
{
    /**
     * @dataProvider provideCases
     */
    public function testFix($expected, $input = null)
    {
        $this->makeTest($expected, $input);
    }

    public function provideCases()
    {
        return [
            [
                <<<'EOF'
<?php

$baz = 0;

if (true === $condition) {
    echo "Do something";
}

$foo = 'bar';
EOF
                    ,
                    <<<'EOF'
<?php

$baz = 0; if (true === $condition) {
    echo "Do something";
} $foo = 'bar';
EOF
            ],
            [
                <<<'EOF'
<?php

$i = 0;

if (true === $condition) {
    echo "Do something";
}

$foo = 'bar';
EOF
                    ,
                    <<<'EOF'
<?php

$i = 0;
if (true === $condition) {
    echo "Do something";
}
$foo = 'bar';
EOF
            ],
            [
                <<<'EOF'
<?php

function doSomething()
{
    $i = 0;

    if (true === $condition) {
        echo "Do something";
    }

    $foo = 'bar';
}
EOF
                    ,
                    <<<'EOF'
<?php

function doSomething()
{
    $i = 0;
    if (true === $condition) {
        echo "Do something";
    }
    $foo = 'bar';
}
EOF
            ],
            [
                <<<'EOF'
<?php

function doSomething()
{
    $i = 0;

    // Comment about structure
    if (true === $condition) {
        echo "Do something";
    }

    $foo = 'bar';
}
EOF
                    ,
                    <<<'EOF'
<?php

function doSomething()
{
    $i = 0;
    // Comment about structure
    if (true === $condition) {
        echo "Do something";
    }
    $foo = 'bar';
}
EOF
            ],
            [
                <<<'EOF'
<?php

function doSomething()
{
    $i = 0;

    // Multi-line comment 
    // about structure
    if (true === $condition) {
        echo "Do something";
    }

    $foo = 'bar';
}
EOF
                    ,
                    <<<'EOF'
<?php

function doSomething()
{
    $i = 0;
    // Multi-line comment 
    // about structure
    if (true === $condition) {
        echo "Do something";
    }
    $foo = 'bar';
}
EOF
            ],
            [
                <<<'EOF'
<?php

function doSomething()
{
    $i = 0;

    /*
     * Comment about control structure
     */
    if (true === $condition) {
        echo "Do something";
    }

    $foo = 'bar';
}
EOF
                    ,
                    <<<'EOF'
<?php

function doSomething()
{
    $i = 0;
    /*
     * Comment about control structure
     */
    if (true === $condition) {
        echo "Do something";
    }
    $foo = 'bar';
}
EOF
            ],
            [
                <<<'EOF'
<?php

function doSomething()
{
    $i = 0;

    /**
     * Comment about control structure
     */
    if (true === $condition) {
        echo "Do something";
    }

    $foo = 'bar';
}
EOF
                    ,
                    <<<'EOF'
<?php

function doSomething()
{
    $i = 0;
    /**
     * Comment about control structure
     */
    if (true === $condition) {
        echo "Do something";
    }
    $foo = 'bar';
}
EOF
            ],
            [
                <<<'EOF'
<?php

$i = 0;

foreach ($vars as $var) {
    echo "Do something";
}

$foo = 'bar';
EOF
                    ,
                    <<<'EOF'
<?php

$i = 0;
foreach ($vars as $var) {
    echo "Do something";
}
$foo = 'bar';
EOF
            ],
            [
                <<<'EOF'
<?php

$i = 0;

for ($i = 0; $i < (10 + 1); ++$i) {
    echo "Do something";
}

$foo = 'bar';
EOF
                    ,
                    <<<'EOF'
<?php

$i = 0;
for ($i = 0; $i < (10 + 1); ++$i) {
    echo "Do something";
}
$foo = 'bar';
EOF
            ],
            [
                <<<'EOF'
<?php

$i = 0;

switch ($var) {
    case 'Foo':
        break;
    case 'Bar':
        $i = function () {
            echo "Do something";
        };
    default:
        break;
}

$foo = 'bar';
EOF
                    ,
                    <<<'EOF'
<?php

$i = 0;
switch ($var) {
    case 'Foo':
        break;
    case 'Bar':
        $i = function () {
            echo "Do something";
        };
    default:
        break;
}
$foo = 'bar';
EOF
            ],
            [
                <<<'EOF'
<?php

$i = 0;

foreach ($vars as $var) {
    // Comment about structure
    if (true === $condition) {
        echo "Do something";
    }
}

$foo = 'bar';
EOF
                    ,
                    <<<'EOF'
<?php

$i = 0;
foreach ($vars as $var) {
    // Comment about structure
    if (true === $condition) {
        echo "Do something";
    }
}
$foo = 'bar';
EOF
            ],
        ];
    }
}
