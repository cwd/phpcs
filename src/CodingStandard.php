<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs;

use Cwd\PhpCs\Fixer\DeclareStrictTypesFixer;
use Cwd\PhpCs\Fixer\ImportGlobalClassesFixer;
use Cwd\PhpCs\Fixer\NewlineAroundControlStructureFixer;
use Cwd\PhpCs\Fixer\NoAuthorTagFixer;
use Cwd\PhpCs\Fixer\NoBooleanComparisonsFixer;
use Cwd\PhpCs\Fixer\NoDeclareStrictTypesFixer;
use Cwd\PhpCs\Fixer\NoSpaceAfterNullableFixer;
use Cwd\PhpCs\Fixer\PhpDoxInheritDocFixer;
use Cwd\PhpCs\Fixer\SpaceBeforeReturnTypeFixer;

final class CodingStandard
{
    const PHP5_FIXERS = [
        'no_author_tag',
        'no_useless_return',
        'header_comment',
        'newline_after_open_tag',
        'no_useless_else',
        'no_useless_return',
        'ordered_use',
        'phpdoc_order',
        'protected_to_private',
        'short_array_syntax',
        'import_global_classes',
        'no_declare_strict_types',
        'php_dox_inheritdoc',
        'newline_around_control_structure',
        // disabled for now - incompatible with PSR-4 ("Tests" rewritten to "tests")
        '-psr0',
    ];

    const PHP7_FIXERS = [
        'no_author_tag',
        'no_useless_return',
        'header_comment',
        'newline_after_open_tag',
        'no_useless_else',
        'no_useless_return',
        'ordered_use',
        'phpdoc_order',
        'protected_to_private',
        'short_array_syntax',
        'import_global_classes',
        'declare_strict_types',
        'space_before_return_type',
        'no_space_after_nullable',
        'php_dox_inheritdoc',
        'newline_around_control_structure',
        // disabled for now - incompatible with PSR-4 ("Tests" rewritten to "tests")
        '-psr0',
    ];

    public static function getCustomFixers()
    {
        return [
            new ImportGlobalClassesFixer(),
            new DeclareStrictTypesFixer(),
            new NoDeclareStrictTypesFixer(),
            new SpaceBeforeReturnTypeFixer(),
            new NoSpaceAfterNullableFixer(),
            new PhpDoxInheritDocFixer(),
            new NoAuthorTagFixer(),
            new NewlineAroundControlStructureFixer(),
            new NoBooleanComparisonsFixer(),
        ];
    }

    private function __construct()
    {
    }
}
