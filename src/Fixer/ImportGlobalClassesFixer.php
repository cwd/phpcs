<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Fixer;

use SplFileInfo;
use Symfony\CS\FixerInterface;
use Symfony\CS\Tokenizer\Token;
use Symfony\CS\Tokenizer\Tokens;

class ImportGlobalClassesFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(SplFileInfo $file, $content)
    {
        $tokens = Tokens::fromCode($content);
        $addedUses = [];

        foreach ($tokens as $index => $token) {
            if ($token->isGivenKind([T_EXTENDS, T_IMPLEMENTS])) {
                $startIndex = $tokens->getNextMeaningfulToken($index);
                $endIndex = $tokens->getNextTokenOfKind($startIndex, [[T_WHITESPACE]]) - 1;

                if (null !== $startIndex && null !== $endIndex) {
                    $this->importClassyName($tokens, $startIndex, $endIndex, $addedUses);
                }

                continue;
            }

            if ($token->isGivenKind([T_DOUBLE_COLON])) {
                $endIndex = $index - 1;
                $startIndex = $this->getFurthestPrevOfKind($tokens, $endIndex, [[T_STRING], [T_NS_SEPARATOR]]);

                if (null !== $startIndex) {
                    $this->importClassyName($tokens, $startIndex, $endIndex, $addedUses);
                }

                continue;
            }

            if ($token->isGivenKind([T_NEW])) {
                $startIndex = $tokens->getNextMeaningfulToken($index);
                $endIndex = $this->getFurthestNextOfKind($tokens, $startIndex, [[T_STRING], [T_NS_SEPARATOR]]);

                if (null !== $startIndex && null !== $endIndex) {
                    $this->importClassyName($tokens, $startIndex, $endIndex, $addedUses);
                }

                continue;
            }

            if ($token->isGivenKind([T_FUNCTION])) {
                $argStartIndex = $tokens->getNextTokenOfKind($index, ['(']);

                while (')' !== $tokens[$argStartIndex]->getContent()) {
                    $maybeTypeHint = $tokens->getNextMeaningfulToken($argStartIndex);

                    if ($tokens[$maybeTypeHint]->isGivenKind([T_STRING, T_NS_SEPARATOR])) {
                        $startIndex = $maybeTypeHint;
                        $endIndex = $this->getFurthestNextOfKind($tokens, $startIndex, [[T_STRING], [T_NS_SEPARATOR]]);

                        if (null !== $endIndex) {
                            $this->importClassyName($tokens, $startIndex, $endIndex, $addedUses);
                        }
                    }

                    $argStartIndex = $tokens->getNextTokenOfKind($argStartIndex, [',', ')']);
                }
            }

            if ($token->isGivenKind(T_DOC_COMMENT)) {
                $token->setContent(preg_replace_callback(
                    '/(@(param|return) )\\\\(\w+)(\s)/',
                    function (array $match) use (&$addedUses) {
                        $addedUses[$match[3]] = true;

                        return $match[1].$match[3].$match[4];
                    },
                    $token->getContent()
                ));
            }
        }

        if (count($addedUses) > 0) {
            // Get the location where to insert the new use statements
            $existingUseIndexes = $tokens->getImportUseIndexes();

            // Remove existing imports from $addedUses
            foreach ($existingUseIndexes as $useIndex) {
                $classyIndex = $tokens->getNextMeaningfulToken($useIndex);
                $classyName = '';

                while ($tokens[$classyIndex]->equalsAny([[T_STRING], [T_NS_SEPARATOR]])) {
                    $classyName .= $tokens[$classyIndex]->getContent();
                    ++$classyIndex;
                }

                unset($addedUses[$classyName]);
            }

            if (count($existingUseIndexes) > 0) {
                $isFollowedByUseStmt = true;
                $firstUseIndex = current($existingUseIndexes);
            } else {
                $isFollowedByUseStmt = false;
                $firstClassIndex = $tokens->getNextTokenOfKind(0, [[T_CLASS], [T_INTERFACE], [T_TRAIT], [T_FUNCTION]]);

                if (null !== $firstClassIndex) {
                    // Class preceded by doc comment?
                    $indexBeforeClass = $tokens->getPrevNonWhitespace($firstClassIndex);
                    $firstUseIndex = $tokens[$indexBeforeClass]->isGivenKind([T_DOC_COMMENT])
                        ? $indexBeforeClass
                        : $firstClassIndex;
                } else {
                    $openTagIndex = $tokens->getNextTokenOfKind(-1, [[T_OPEN_TAG]]);
                    $firstUseIndex = $tokens->getNextMeaningfulToken($openTagIndex);
                }
            }

            krsort($addedUses);
            $i = 0;
            $l = count($addedUses);

            foreach ($addedUses as $addedUse => $_) {
                $isLast = $i === $l - 1;

                $tokens->insertAt($firstUseIndex, [
                    new Token([T_USE, 'use']),
                    new Token([T_WHITESPACE, ' ']),
                    new Token([T_STRING, $addedUse]),
                    new Token(';'),
                    new Token([T_WHITESPACE, $isLast && $isFollowedByUseStmt ? "\n" : "\n\n"]),
                ]);

                ++$i;
            }
        }

        return $tokens->generateCode();
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Global classes must be imported throw a "use" statement.';
    }

    /**
     * @inheritdoc
     */
    public function getLevel()
    {
        return self::CONTRIB_LEVEL;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'import_global_classes';
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function supports(SplFileInfo $file)
    {
        return true;
    }

    private function getFurthestNextOfKind(Tokens $tokens, $index, array $expectedTokens = [], $caseSensitive = true)
    {
        return $this->getFurthestSiblingOfKind($tokens, $index, 1, $expectedTokens, $caseSensitive);
    }

    private function getFurthestPrevOfKind(Tokens $tokens, $index, array $expectedTokens = [], $caseSensitive = true)
    {
        return $this->getFurthestSiblingOfKind($tokens, $index, -1, $expectedTokens, $caseSensitive);
    }

    private function getFurthestSiblingOfKind(Tokens $tokens, $index, $direction, array $expectedTokens = [], $caseSensitive = true)
    {
        $furthestIndex = null;

        for ($i = $index + $direction; $i >= 0; $i += $direction) {
            if (!$tokens[$i]->equalsAny($expectedTokens, $caseSensitive)) {
                break;
            }

            $furthestIndex = $i;
        }

        return $furthestIndex;
    }

    private function importClassyName(Tokens $tokens, $startIndex, $endIndex, array &$addedUses)
    {
        if ('\\' === $tokens[$startIndex]->getContent() && $endIndex === $startIndex + 1) {
            $tokens[$startIndex]->clear();
            $classyName = $tokens[$endIndex]->getContent();
            $addedUses[$classyName] = true;
        }
    }
}
