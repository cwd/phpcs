<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Fixer;

use SplFileInfo;
use Symfony\CS\FixerInterface;
use Symfony\CS\Tokenizer\Token;
use Symfony\CS\Tokenizer\Tokens;

class NoBooleanComparisonsFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(SplFileInfo $file, $content)
    {
        $tokens = Tokens::fromCode($content);

        foreach ($tokens as $index => $token) {
            if (!$token->isGivenKind([T_IS_EQUAL, T_IS_IDENTICAL, T_IS_NOT_EQUAL, T_IS_NOT_IDENTICAL])) {
                continue;
            }

            $previousIndex = $tokens->getPrevMeaningfulToken($index);
            $nextIndex = $tokens->getNextMeaningfulToken($index);

            if ($this->isTrue($tokens[$previousIndex])) {
                $tokens[$previousIndex]->clear();
                $this->clearWhitespaceBetween($tokens, $previousIndex, $index);
                $this->clearWhitespaceBetween($tokens, $index, $nextIndex);

                if ($token->isGivenKind([T_IS_EQUAL, T_IS_IDENTICAL])) {
                    $token->clear();
                } else {
                    $tokens[$index] = new Token('!');
                }

                continue;
            }

            if ($this->isTrue($tokens[$nextIndex])) {
                $tokens[$nextIndex]->clear();
                $this->clearWhitespaceBetween($tokens, $index, $nextIndex);
                $this->clearWhitespaceBetween($tokens, $previousIndex, $index);

                if ($token->isGivenKind([T_IS_NOT_EQUAL, T_IS_NOT_IDENTICAL])) {
                    $tokens->insertAt($previousIndex, new Token('!'));
                }

                $token->clear();

                continue;
            }

            if ($this->isFalse($tokens[$previousIndex])) {
                $tokens[$previousIndex]->clear();
                $this->clearWhitespaceBetween($tokens, $previousIndex, $index);
                $this->clearWhitespaceBetween($tokens, $index, $nextIndex);

                if ($token->isGivenKind([T_IS_NOT_EQUAL, T_IS_NOT_IDENTICAL])) {
                    $token->clear();
                } else {
                    $tokens[$index] = new Token('!');
                }

                continue;
            }

            if ($this->isFalse($tokens[$nextIndex])) {
                $tokens[$nextIndex]->clear();
                $this->clearWhitespaceBetween($tokens, $index, $nextIndex);
                $this->clearWhitespaceBetween($tokens, $previousIndex, $index);

                if ($token->isGivenKind([T_IS_EQUAL, T_IS_IDENTICAL])) {
                    $tokens->insertAt($previousIndex, new Token('!'));
                }

                $token->clear();

                continue;
            }
        }

        return $tokens->generateCode();
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'One should never explicitly compare with true and false.';
    }

    /**
     * @inheritdoc
     */
    public function getLevel()
    {
        return self::CONTRIB_LEVEL;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'no_boolean_comparisons';
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function supports(SplFileInfo $file)
    {
        return true;
    }

    private function isTrue(Token $token)
    {
        return $token->isGivenKind(T_STRING) && 'true' === $token->getContent();
    }

    private function isFalse(Token $token)
    {
        return $token->isGivenKind(T_STRING) && 'false' === $token->getContent();
    }

    private function clearWhitespaceBetween(Tokens $tokens, $beforeIndex, $afterIndex)
    {
        $index = $beforeIndex + 1;

        while ($index < $afterIndex) {
            if ($tokens[$index]->isWhitespace()) {
                $tokens[$index]->clear();
            }

            ++$index;
        }
    }
}
