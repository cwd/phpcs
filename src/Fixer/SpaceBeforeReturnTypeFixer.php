<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Fixer;

use SplFileInfo;
use Symfony\CS\FixerInterface;
use Symfony\CS\Tokenizer\Token;
use Symfony\CS\Tokenizer\Tokens;

class SpaceBeforeReturnTypeFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(SplFileInfo $file, $content)
    {
        $tokens = Tokens::fromCode($content);

        foreach ($tokens as $index => $token) {
            if (!$token->isGivenKind([T_FUNCTION])) {
                continue;
            }

            $closingBraceIndex = $tokens->getNextTokenOfKind($index, [')']);
            $maybeColonIndex = $tokens->getNextMeaningfulToken($closingBraceIndex);

            if (null === $maybeColonIndex) {
                continue;
            }

            if (':' !== $tokens[$maybeColonIndex]->getContent()) {
                continue;
            }

            $tokens->removeLeadingWhitespace($maybeColonIndex);

            if ($tokens[$maybeColonIndex + 1]->isWhitespace()) {
                // Set whitespace after ":" to " "
                $tokens[$maybeColonIndex + 1]->setContent(' ');

                continue;
            }

            // Insert space after ":"
            $tokens->insertAt($maybeColonIndex + 1, [
                new Token([T_WHITESPACE, ' ']),
            ]);
        }

        return $tokens->generateCode();
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'The return type must be preceded by exactly one space.';
    }

    /**
     * @inheritdoc
     */
    public function getLevel()
    {
        return self::CONTRIB_LEVEL;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'space_before_return_type';
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function supports(SplFileInfo $file)
    {
        return true;
    }
}
