<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Fixer;

use SplFileInfo;
use Symfony\CS\FixerInterface;
use Symfony\CS\Tokenizer\Tokens;

class PhpDoxInheritDocFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(SplFileInfo $file, $content)
    {
        $tokens = Tokens::fromCode($content);

        foreach ($tokens as $index => $token) {
            if (!$token->isGivenKind([T_DOC_COMMENT])) {
                continue;
            }

            $token->setContent(str_replace('{@inheritdoc}', '@inheritdoc', $token->getContent()));
        }

        return $tokens->generateCode();
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'The @inheritdoc tag must be written without curly braces.';
    }

    /**
     * @inheritdoc
     */
    public function getLevel()
    {
        return self::CONTRIB_LEVEL;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'php_dox_inheritdoc';
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        // Run after PhpdocInlineTagFixer
        return -1;
    }

    /**
     * @inheritdoc
     */
    public function supports(SplFileInfo $file)
    {
        return true;
    }
}
