<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Fixer;

use SplFileInfo;
use Symfony\CS\FixerInterface;
use Symfony\CS\Tokenizer\Tokens;

class NoSpaceAfterNullableFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(SplFileInfo $file, $content)
    {
        $tokens = Tokens::fromCode($content);

        foreach ($tokens as $index => $token) {
            if (!$token->isGivenKind([T_FUNCTION])) {
                continue;
            }

            $argIndex = $tokens->getNextTokenOfKind($index, ['(']);

            if (null === $argIndex) {
                continue;
            }

            while (null !== $argIndex && !$tokens[$argIndex]->equals(')')) {
                $maybeNullableIndex = $tokens->getNextMeaningfulToken($argIndex);

                if ($tokens[$maybeNullableIndex]->equals('?')) {
                    $tokens->removeTrailingWhitespace($maybeNullableIndex);
                }

                $argIndex = $tokens->getNextTokenOfKind($argIndex, [',', ')']);
            }

            if (null === $argIndex) {
                continue;
            }

            $maybeColonIndex = $tokens->getNextMeaningfulToken($argIndex);

            if (null === $maybeColonIndex || !$tokens[$maybeColonIndex]->equals(':')) {
                continue;
            }

            $maybeNullableIndex = $tokens->getNextMeaningfulToken($maybeColonIndex);

            if (null === $maybeNullableIndex) {
                continue;
            }

            if ($tokens[$maybeNullableIndex]->equals('?')) {
                $tokens->removeTrailingWhitespace($maybeNullableIndex);
            }
        }

        return $tokens->generateCode();
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'The "?" in nullable type hints must not be followed by whitespace.';
    }

    /**
     * @inheritdoc
     */
    public function getLevel()
    {
        return self::CONTRIB_LEVEL;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'no_space_after_nullable';
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function supports(SplFileInfo $file)
    {
        return true;
    }
}
