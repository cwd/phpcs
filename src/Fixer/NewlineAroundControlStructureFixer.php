<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Fixer;

use SplFileInfo;
use Symfony\CS\FixerInterface;
use Symfony\CS\Tokenizer\Token;
use Symfony\CS\Tokenizer\Tokens;

class NewlineAroundControlStructureFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(SplFileInfo $file, $content)
    {
        $tokens = Tokens::fromCode($content);
        $skip = 0;

        foreach ($tokens as $index => $token) {
            if ($skip > 0) {
                --$skip;
                continue;
            }

            if (!$token->isGivenKind([T_IF, T_FOR, T_FOREACH, T_SWITCH])) {
                continue;
            }

            $previousIndex = $index - 1;

            // If the control structure is preceded by a comment, add the empty
            // line before the comment
            $previousNonWhitespaceIndex = $tokens->getPrevNonWhitespace($index);

            while ($tokens[$previousNonWhitespaceIndex]->isComment()) {
                $previousIndex = $previousNonWhitespaceIndex - 1;
                $previousNonWhitespaceIndex = $tokens->getPrevNonWhitespace($previousNonWhitespaceIndex);
            }

            // No newline is needed if the previous token is the beginning of
            // a block
            if ('{' !== $tokens[$previousNonWhitespaceIndex]->getContent()) {
                if ($tokens[$previousIndex]->isWhitespace()) {
                    $this->ensureEmptyLine($tokens[$previousIndex]);
                } else {
                    $tokens->insertAt($previousIndex + 1, [
                        new Token([T_WHITESPACE, "\n\n"]),
                    ]);

                    // We insert at the previous index, so the next index in the
                    // loop is going to be the same control structure
                    // Skip that index
                    $skip = 1;

                    // The next index changed now
                    ++$index;
                }
            }

            $openingBraceIndex = $tokens->getNextTokenOfKind($index, ['{']);

            if (null === $openingBraceIndex) {
                continue;
            }

            $closingBraceIndex = $this->findClosingBrace($tokens, $openingBraceIndex);

            if (null === $closingBraceIndex) {
                continue;
            }

            $nextIndex = $closingBraceIndex + 1;
            $nextNonWhitespaceIndex = $tokens->getNextNonWhitespace($closingBraceIndex);

            if ('}' !== $tokens[$nextNonWhitespaceIndex]->getContent()) {
                if ($tokens[$nextIndex]->isWhitespace()) {
                    $this->ensureEmptyLine($tokens[$nextIndex]);
                } else {
                    $tokens->insertAt($nextIndex, [
                        new Token([T_WHITESPACE, "\n\n"]),
                    ]);
                }
            }
        }

        return $tokens->generateCode();
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Control structures must be surrounded by empty lines.';
    }

    /**
     * @inheritdoc
     */
    public function getLevel()
    {
        return self::CONTRIB_LEVEL;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'newline_around_control_structure';
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        // Run before the regular fixers
        return 10;
    }

    /**
     * @inheritdoc
     */
    public function supports(SplFileInfo $file)
    {
        return true;
    }

    /**
     * Returns index of the closing curly brace.
     *
     * @param Tokens $tokens The tokens
     * @param int    $index  The index where to start looking
     *
     * @return int|null The index of the closing brace or null if none is found
     */
    private function findClosingBrace(Tokens $tokens, $index)
    {
        $nestingLevel = 1;

        while (true) {
            $index = $tokens->getNextTokenOfKind($index, ['{', '}']);

            if (null === $index) {
                return null;
            }

            if ('{' === $tokens[$index]->getContent()) {
                ++$nestingLevel;

                continue;
            }

            --$nestingLevel;

            if (0 === $nestingLevel) {
                return $index;
            }
        }

        return null;
    }

    /**
     * Ensures that the given whitespace token contains an empty line.
     *
     * @param Token $token The whitespace token
     */
    private function ensureEmptyLine(Token $token)
    {
        $content = $token->getContent();
        $newlineCount = substr_count($content, "\n");

        if (0 === $newlineCount) {
            // If there is no new line yet (only spaces), completely trim all
            // the spaces and replace them by an empty line
            $token->setContent("\n\n");
        } elseif (1 === $newlineCount) {
            // Otherwise prepend the spaces (=indentation) by the necessary
            // amount of empty lines
            $token->setContent("\n".$content);
        }
    }
}
