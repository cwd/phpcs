<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Fixer;

use SplFileInfo;
use Symfony\CS\FixerInterface;
use Symfony\CS\Tokenizer\Token;
use Symfony\CS\Tokenizer\Tokens;

class DeclareStrictTypesFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(SplFileInfo $file, $content)
    {
        $tokens = Tokens::fromCode($content);
        $openTagIndex = $tokens->getNextTokenOfKind(-1, [[T_OPEN_TAG]]);
        $declareIndex = $tokens->getNextTokenOfKind($openTagIndex, [[T_DECLARE]]);
        $fileCommentIndex = $tokens->getNextTokenOfKind($openTagIndex, [[T_COMMENT]]);
        $hasFileComment = null !== $fileCommentIndex;

        // Existing declare - check position
        if (null !== $declareIndex) {
            $positionedCorrectly = isset($tokens[$declareIndex - 1])
                && $tokens[$declareIndex - 1]->isWhitespace()
                && "\n\n" === $tokens[$declareIndex - 1]->getContent()
                && (
                    ($hasFileComment && $declareIndex === $fileCommentIndex)
                    || (!$hasFileComment && $declareIndex === $openTagIndex)
                );

            if ($positionedCorrectly) {
                return $tokens->generateCode();
            }

            // Remove and reposition
            $declareEndIndex = $tokens->getNextTokenOfKind($declareIndex, [';']);

            for ($i = $declareIndex; $i <= $declareEndIndex; ++$i) {
                $tokens[$i]->clear();
            }

            $tokens->removeTrailingWhitespace($declareEndIndex);
        }

        $beforeInsertionIndex = $hasFileComment ? $fileCommentIndex : $openTagIndex;
        $insertionIndex = $beforeInsertionIndex + 1;

        // The "<?php" token may include a trailing newline (??)
        $extraLeadingNewline = "\n" !== substr($tokens[$beforeInsertionIndex]->getContent(), -1);

        $tokens->removeTrailingWhitespace($beforeInsertionIndex);

        $tokens->insertAt($insertionIndex, [
            new Token([T_WHITESPACE, $extraLeadingNewline ? "\n\n" : "\n"]),
            new Token([T_DECLARE, 'declare']),
            new Token('('),
            new Token([T_STRING, 'strict_types']),
            new Token('='),
            new Token([T_LNUMBER, '1']),
            new Token(')'),
            new Token(';'),
            new Token([T_WHITESPACE, "\n\n"]),
        ]);

        return $tokens->generateCode();
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'The declare(strict_types=1) statement must exist at the beginning of the file.';
    }

    /**
     * @inheritdoc
     */
    public function getLevel()
    {
        return self::CONTRIB_LEVEL;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'declare_strict_types';
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function supports(SplFileInfo $file)
    {
        return true;
    }
}
