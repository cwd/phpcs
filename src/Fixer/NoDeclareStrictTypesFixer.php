<?php

/*
 * This file is part of the CWD PHP Coding Standard.
 *
 * (c) 2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\PhpCs\Fixer;

use SplFileInfo;
use Symfony\CS\FixerInterface;
use Symfony\CS\Tokenizer\Tokens;

class NoDeclareStrictTypesFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(SplFileInfo $file, $content)
    {
        $tokens = Tokens::fromCode($content);
        $openTagIndex = $tokens->getNextTokenOfKind(-1, [[T_OPEN_TAG]]);
        $declareIndex = $tokens->getNextTokenOfKind($openTagIndex, [[T_DECLARE]]);

        // Existing declare - check position
        if (null !== $declareIndex) {
            // Remove and reposition
            $declareEndIndex = $tokens->getNextTokenOfKind($declareIndex, [';']);

            for ($i = $declareIndex; $i <= $declareEndIndex; ++$i) {
                $tokens[$i]->clear();
            }

            $tokens->removeTrailingWhitespace($declareEndIndex);
        }

        return $tokens->generateCode();
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'The declare(strict_types=1) statement must not exist.';
    }

    /**
     * @inheritdoc
     */
    public function getLevel()
    {
        return self::CONTRIB_LEVEL;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'no_declare_strict_types';
    }

    /**
     * @inheritdoc
     */
    public function getPriority()
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function supports(SplFileInfo $file)
    {
        return true;
    }
}
